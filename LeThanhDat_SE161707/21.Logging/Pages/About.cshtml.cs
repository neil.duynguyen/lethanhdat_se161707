using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace _21.Logging.Pages
{
    public class AboutModel : PageModel
    {
        private readonly ILogger<AboutModel> _logger;

        public AboutModel(ILogger<AboutModel> logger)
        {
            _logger = logger;
        }

        public void OnGet()
        {
            _logger.LogInformation(1002, "About page visited at {DT}",
            DateTime.UtcNow.ToLongTimeString());
            _logger.LogInformation(MyLogEvents.GetItem,"[With event ID] About page visited at {DT}",
            DateTime.UtcNow.ToLongTimeString());
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace _21.Logging.Pages
{
    public class PrivacyModel : PageModel
    {
        private readonly ILogger _logger;

        public PrivacyModel(ILoggerFactory logger)
        {
            _logger = logger.CreateLogger("MyCategory");
        }

        public void OnGet()
        {
            _logger.LogInformation("GET Pages.ContactModel called.");
        }
    }
}
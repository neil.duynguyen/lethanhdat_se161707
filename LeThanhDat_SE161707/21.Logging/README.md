Project này để học về Logging trong ASP.NET Core

__Lí_thuyết
	Mặc định host và web app đã có sẵn hệ thống log rồi

	Để có thể sử dụng log trong model hoặc service, ta sẽ inject ILogger vào đối tượng cần log
	VD
	    public class AboutModel : PageModel
        {
            private readonly ILogger _logger;

            public AboutModel(ILogger<AboutModel> logger)
            {
                _logger = logger;
            }

            public void OnGet()
            {
                _logger.LogInformation("About page visited at {DT}", 
                    DateTime.UtcNow.ToLongTimeString());
            }
        }

    Thông thường, một log sẽ có cấu trúc như sau
        [{LEVEL}]: {CATEGORY} [EVENT_ID]
            {MESSAGE}
    VD
        info: Microsoft.Hosting.Lifetime[0]
            Application started. Press Ctrl+C to shut down.

    Để log với phân loại, chúng ta sẽ inject ILoggerFactory
        public class ContactModel : PageModel
        {
            private readonly ILogger _logger;

            public ContactModel(ILoggerFactory logger)
            {
                _logger = logger.CreateLogger("MyCategory"); // Custom category
            }

            public void OnGet()
            {
                _logger.LogInformation("GET Pages.ContactModel called.");
            }
        }
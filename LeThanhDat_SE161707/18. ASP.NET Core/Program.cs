using _18._ASP.NET_Core.Middlewares;

var builder = WebApplication.CreateBuilder(args);


// DEPENDENCY INJECTION USING DI CONTAINER
// Add services to the container
#region Dependency Injection
builder.Services.AddRazorPages();
builder.Services.AddControllersWithViews();
#endregion

var app = builder.Build();

// Configure the HTTP request pipeline.
#region Middleware
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapGet("/hi", () => "Hello!");

app.MapDefaultControllerRoute();
app.MapRazorPages();

// Use custom middleware with 2 ways
// app.UseMiddleware<MyCustomMiddleware>(); 
app.UseMyCustomMiddleware();
// Map request path with accosiate middleware
app.Map("/level", levelApp => {
    levelApp.Map("/map1", HandleMapTest1);

    levelApp.Map("/map2", HandleMapTest2);
});

app.Use(async (context, next) =>
{
    // Do work that can write to the Response.
    await next.Invoke(); // Invoke next delegate in pipeline
    // Do logging or other work that doesn't write to the Response.
});

// The first Run method with request delegate will terminate the request pipeline
app.Run(async context =>
{
    await context.Response.WriteAsync("Hello from non-Map delegate.");
});

static void HandleMapTest1(IApplicationBuilder app)
{
    app.Run(async context =>
    {
        await context.Response.WriteAsync("Map Test 1");
    });
}

static void HandleMapTest2(IApplicationBuilder app)
{
    app.Run(async context =>
    {
        await context.Response.WriteAsync("Map Test 2");
    });
}
#endregion

app.Run();

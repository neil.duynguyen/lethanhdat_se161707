Project này để học về Middleware và Request Pipeline

__Lí_thuyết
    Request pipeline là một cơ chế xử lý một request đầu vào và kết thúc với đầu ra là một response. 
    Pipeline chỉ ra cách mà ứng dụng phản hồi với HTTP Request.

    Cách thức hoạt động
        Đầu tiên, HTTP Request đến. 
        Web server nhặt lấy request và tạo một HttpContext và gán nó vào Middleware đầu tiên trong request pipeline.
        Middleware đầu tiên sẽ nhận request, xử lý và gán nó cho middleware tiếp theo. Quá trình này tiếp diễn cho đến khi đi đến middleware cuối cùng. 
        Middleware cuối cùng sẽ trả request ngược lại cho middleware trước đó

        Mỗi Middleware trong pipeline sẽ tuần tự có cơ hội thứ hai để kiểm tra lại request và điểm chỉnh response trước khi được trả lại.
        Cuối cùng, response sẽ đến Kestrel nó sẽ trả response về cho client. 
    
    Để xây dựng request pipeline, ta sử dụng các request delegate, chúng xử lý từng HTTP request.

    Các request delegate được cấu hình thông qua việc sử dụng các extension method như Run, Map và Use. 
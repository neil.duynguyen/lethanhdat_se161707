Project này để học về dependency injection

__Lí_thuyết
    Dependency là việc một đối tượng được đối tượng khác phụ thuộc, thông qua việc có sử dụng chức năng của lớp này
    VD:
    class Lesson 
    {
        public void Learn() 
        {
            // some code
        }
        
    }
    class Student
    {
        public void Learn() 
        {
            Lesson lesson = new Lesson(); // Tự tạo đối tượng Lesson mới để sử dụng
            lesson.Learn(); // Lesson là dependency của Student, vì Student có sử dụng một chức năng của Lesson
        }
    }

    Trong ví dụ trên có một vấn đề, nếu class Lesson bị thay đổi, class Student sẽ bị thay đổi theo
    VD: 
    class Lesson 
    {
        //Thêm tham số vào phương thức Learn ban đầu
        public void Learn(string subject)  
        {
            // some code
        }
        
    }
    class Student
    {
        public void Learn() 
        {
            Lesson lesson = new Lesson(); // Tự tạo đối tượng Lesson mới để sử dụng
            lesson.Learn("Math"); // Khi phương thức Learn bị chỉnh sửa, code ở class Student sẽ phải chỉnh sửa theo
        }
    }

    DEPENDENCY INJECTION là kĩ thuật trong lập trình để sử dụng các lớp phụ thuộc
        Chúng ta sẽ không khởi tạo các lớp phụ thuộc đó ở trong lớp hiện tại, thay vào đó, có 3 cách
            Constructor injection
            Setter injection
            Interface injection

        VD: lấy constructor injection làm ví dụ
        class Lesson 
        {
            //Thêm tham số vào phương thức Learn ban đầu
            public void Learn(string subject)  
            {
                // some code
            }
        
        }
        class Student
        {
            Lesson lesson; // Lesson là dependency của Student

            // Dependency được đưa vào Student bằng constructor
            public Student(Lesson lesson) => this.lesson = lesson
            public void Learn() 
            {
                //Sử dụng dependency đã được inject 
                lesson.Learn();
            }
        }

    Lợi ích của Dependency injection
        Dễ bảo trì
        Dễ mở rộng
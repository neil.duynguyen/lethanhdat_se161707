Project này để học về method
__Định_nghĩa
	method (phương thức) là đoạn code được chạy khi được yêu cầu, và sẽ làm các công việc nhất định
__Mục_tiêu

__Lí_thuyết
	Method được định nghĩa như sau
	<Access Modifiers> <return type> <name_method>(<parameters>)
	{
		// Các câu lệnh trong phương thức
	}

	Để sử dụng hàm trong chương trình, chỉ cần gọi tên và truyền đối số (arguments) vào

	Về tham số (Parameters)
		Được khai báo sau tên method, trong dấu ngoặc kép
		Một method có thể chứa một hoặc nhiều tham số, hoặc không có tham số
		Default Parameters
			Có thể khởi tạo giá trị mặc định cho tất cả tham số bằng cách dùng dấu bằng. 
			Khi gọi phương thức không sử dụng đối số, sẽ sử dụng tham số mặc định 
		VD
		//2 params
		int Sum(int num1, int num2)
		{
			...
		}

		//1 param
		int Square(int num)
		{
			...
		}

		//No param
		int GetPiValue()
		{
			...
		}

		//Default param
		int RectangleArea(int side1 = 3, side2 = 4)
		{
			...
		}

	Return Values
		Một phương thức sẽ có giá trị trả về, và kiểu dữ liệu của giá trị trả về (return type) được định nghĩa trước tên hàm
		Từ khóa là return
		Khi kiểu trả về là void, thì phương thức sẽ không cần return
		int Sum(int num1, int num2)
		{
			return num1+num2;
		}

	Named Arguments
		Khi gọi một phương thức, chúng ta cũng có thể truyền đối số theo cú pháp key: value
		Khi đó, có thể thay đổi thứ tự truyền đối số cho phương thức
		VD
		static void MyMethod(string child1, string child2, string child3) 
		{
		  Console.WriteLine("The youngest child is: " + child3);
		}

		static void Main(string[] args)
		{
		  MyMethod(child3: "John", child1: "Liam", child2: "Liam");
		}

		// The youngest child is: John

﻿
//define method
int Power(int number)
{
    return number * number;
}

Console.WriteLine(Power(3));
string CreateBankAccount(int id = 1, string name = "DatUwU", string address = "VN")
{
    return $"New account info: {id}, {name}, {address}";
}

//Named Arguments
Console.WriteLine(CreateBankAccount(name: "DatUwU", id: 1, address: "VN"));
//Default parameter
Console.WriteLine(CreateBankAccount(name: "UwU", address: "HCM"));
Console.WriteLine(CreateBankAccount());
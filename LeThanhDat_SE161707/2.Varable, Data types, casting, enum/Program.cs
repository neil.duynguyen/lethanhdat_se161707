﻿using System.Reflection.Emit;

namespace _2.Varable__Data_types__casting__enum
{
    //Declate enum
    enum Direction
    {
        North,
        South,
        West,
        East,
    }

    class Program
    {
        static void Main(string[] args)
        {
            //Declare variable
            int age = 10;
            string name = "Dat UwU";
            double grade = 9.5;
            bool isPassed = true;

            Direction direction = Direction.East;
            Console.WriteLine(age);
            Console.WriteLine(name);
            Console.WriteLine(grade);
            Console.WriteLine(isPassed);
            Console.WriteLine(direction);
        }
    }

}



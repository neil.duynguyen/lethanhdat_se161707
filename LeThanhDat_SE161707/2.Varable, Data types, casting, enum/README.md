Project này để học về Variables, Data types, Casting, Enum
__Định_nghĩa
	Variables là biến, dùng để lưu trữ dữ liệu 
	Const là hằng, dùng để lưu trữ dữ liệu không đổi trong suốt quá trình chạy chương trình
	Enum là kiểu dữ liệu đặc biệt để lưu trữ tập hợp các hằng số 
	Casting là việc biến đổi dữ liệu thuộc kiểu dữ liệu này thành kiểu dữ liệu khác.
__Mục_tiêu

__Lí_thuyết
	Để lưu trữ dữ liệu vào biến, cần có các kiểu dữ liệu (data types) như 
		int, Integer,... để lưu trữ số nguyên
		double, float,... để lưu trữ số thực
		boolean để lưu trữ logic đúng sai
		string để lưu trữ chuỗi

	VD cách khai báo
		int age = 20;
	
	Về casting, C# sẽ ngầm hiểu và tự động casting từ kiểu đơn giản sang kiểu phức tạp
		VD 
		int myInt = 9;
		double myDouble = myInt; // myDouble == 9
	Ngược lại, chúng ta sẽ casting từ kiểu phức tạp sang kiểu đơn giản bằng cách chú thích trước kiểu dữ liệu sắp gán cho giá trị
		VD
		double myDouble = 9.78;
		int myInt = (int) myDouble;  // myInt == 9

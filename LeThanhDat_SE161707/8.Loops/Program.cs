﻿
using System.Security.Cryptography;

//Do while loop
Console.WriteLine("=== While loop");

int mathScore = RandomNumberGenerator.GetInt32(10); ;
while (mathScore < 7) 
{
    Console.WriteLine($"Math score = {mathScore}. You do not pass the exam");
    mathScore = RandomNumberGenerator.GetInt32(10);
}
Console.WriteLine($"Math score = {mathScore}. You pass the exam");


Console.WriteLine("=== for loop");

for (int i = 0; i <= 10; i = i + 2)
{
    Console.WriteLine(i);
}
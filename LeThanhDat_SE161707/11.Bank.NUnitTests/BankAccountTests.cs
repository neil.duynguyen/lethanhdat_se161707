using _11.UnitTestingAndTDD;

namespace _11.Bank.NUnitTests
{
    public class BankAccountTests
    {
        private BankAccount account;
        [SetUp]
        public void Setup()
        {
            //Arrange
            account = new BankAccount(1000);
        }

        [Test]
        public void Add_Funds_Update_Balance()
        {            
            //Act
            account.Add(500);
            //Assert
            Assert.That(account.Balance, Is.EqualTo(1500));
        }

        [Test]
        public void Add_Negative_Funds_Update_Balance()
        {
            //Act + assert
            Assert.Throws<ArgumentOutOfRangeException>(() => account.Add(-500));
        }

        [Test]
        public void Withdraw_Funds_Update_Balance()
        {
            //Act
            account.Withdraw(500);
            //Assert
            Assert.That(account.Balance, Is.EqualTo(500));
        }

        [Test]
        public void Withdraw_Negative_Funds_Update_Balance()
        {
            //Act + assert
            Assert.Throws<ArgumentOutOfRangeException>(() => account.Withdraw(-500));
        }

        [Test]
        public void Transfer_Fund_Update_Both_Account()
        {
            //Arrange
            var otherAccount = new BankAccount(2000);
            //Act
            account.TransferFundsTo(otherAccount, 500);
            //Assert
            Assert.That(account.Balance, Is.EqualTo(500));
            Assert.That(otherAccount.Balance, Is.EqualTo(2500));
        }

        [Test]
        public void Transfer_To_Non_Existing_Account_Throws()
        {
            //Act + Assert
            Assert.Throws<ArgumentNullException>(() => account.TransferFundsTo(null, 500));
        }

        [Test]
        public void Transfer_Negative_Fund_Update_Both_Account()
        {
            //Arrange
            var otherAccount = new BankAccount(2000);
            //Act + assert
            Assert.Throws<ArgumentOutOfRangeException>(() => account.TransferFundsTo(otherAccount, -500));
        }
    }
}
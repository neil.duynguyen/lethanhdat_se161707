﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System.Net.Mime;
using System.Threading.Tasks;

namespace _24.Routing
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class ProductsMiddleware
    {
        private readonly LinkGenerator _linkGenerator;

        public ProductsMiddleware(RequestDelegate next, LinkGenerator linkGenerator) =>
            _linkGenerator = linkGenerator;

        public async Task InvokeAsync(HttpContext httpContext)
        {
            httpContext.Response.ContentType = MediaTypeNames.Text.Plain;

            var productsPath = _linkGenerator.GetPathByAction("Products", "Store");

            await httpContext.Response.WriteAsync(
                $"Go to {productsPath} to see our products.");
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class MiddlewareExtensions
    {
        public static IApplicationBuilder UseProductsMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ProductsMiddleware>();
        }
    }

}

﻿
var number1 = 15;
var number2 = 2.5;

//Arithmetic operator
Console.WriteLine("=== Basic operator");
Console.WriteLine($"number1 + number2 = {number1 + number2}");
Console.WriteLine($"number1 - number2 = {number1 - number2}");
Console.WriteLine($"number1 * number2 = {number1 * number2}");
Console.WriteLine($"number1 / number2 = {number1 / number2}");
Console.WriteLine();

Console.WriteLine("=== ++ and -- operator");
Console.WriteLine($"++number1 = {++number1}");
Console.WriteLine($"--number2 = {--number2}");
Console.WriteLine();

// Assignment operator
Console.WriteLine("=== operator used with = ");
number1 += 3;
Console.WriteLine($"number1 +=3 = {number1}");
number1 -= 3;
Console.WriteLine($"number1 -=3 = {number1}");
number1 *= 3;
Console.WriteLine($"number1 *=3 = {number1}");
number1 /= 3;
Console.WriteLine($"number1 /=3 = {number1}");
Console.WriteLine();

//Comparision and Logical operator
Console.WriteLine("=== Comparision and Logical operator");
var x = 5;
Console.WriteLine($"x > 3 && x < 10? {x > 3 && x < 10}");
Console.WriteLine($"x < 5 || x < 4? {x < 5 || x < 4}");
Console.WriteLine($"!(x < 5 && x < 10)? {!(x < 5 && x < 10)}");
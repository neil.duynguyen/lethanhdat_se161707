﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace _25.Make_HTTP_Request.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HttpClientFactoryController : ControllerBase
    {
        private readonly IHttpClientFactory _httpClientFactory;
        public HttpClientFactoryController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        [HttpGet]
        public async Task<ActionResult> Get()
        {
            var client = _httpClientFactory.CreateClient();
            client.BaseAddress = new Uri("http://api.google.com");
            string result = await client.GetStringAsync("/");
            return Ok(result);
        }
    }
}

﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;

namespace _25.Make_HTTP_Request.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NamedClientsController : ControllerBase
    {
        private readonly IHttpClientFactory _httpClientFactory;
        public NamedClientsController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        [HttpGet]
        public async Task<ActionResult> Get()
        {
            var client = _httpClientFactory.CreateClient("g");
            string result = await client.GetStringAsync("/");
            return Ok(result);
        }
    }
}

﻿using _25.Make_HTTP_Request.TypedClients;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace _25.Make_HTTP_Request.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TypedClientController : ControllerBase
    {
        private readonly TypedCustomClient _typedCustomClient;
        public TypedClientController(TypedCustomClient typedCustomClient)
        {
            _typedCustomClient = typedCustomClient;
        }
        [HttpGet]
        public async Task<ActionResult> Get()
        {
            string result = await _typedCustomClient.Client.GetStringAsync("/");
            return Ok(result);
        }
    }
}

﻿namespace _25.Make_HTTP_Request.TypedClients
{
    public class TypedCustomClient
    {
        public HttpClient Client { get; set;}
        public TypedCustomClient(HttpClient httpClient)
        {
            httpClient.BaseAddress = new Uri("https://api.google.com/");
            httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
            httpClient.DefaultRequestHeaders.Add("User-Agent", "HttpClientFactory-Sample");
            Client = httpClient;
        }
    }

}

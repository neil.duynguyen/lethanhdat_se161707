Project này để học về HTTP Request

__Lí_thuyết
	HTTP Request hiểu một cách đơn giản là các thông tin sẽ được gửi từ khách hàng (client) lên server. Server sẽ có nhiệm vụ tìm và xử lý các loại dữ liệu, thông tin, client mong muốn. 
	
	HTTP Request có thể tồn tại dưới file text hoặc dưới dạng XML hoặc dạng Json. 

	Trong C#, có thể gửi HTTP request bằng lớp HttpClient
	Lớp HttpClient được sử dụng để gửi truy vấn HTTP (Http Request Message - Request) và nhận phản hồi Response (Http Response Message) từ các truy vấn đó.

	Có nhiều phương thức để gửi thông tin bằng Http Request, trong đó có thể kể đến các phương thức phổ biến như
		POST để thêm dữ liệu mới vào bộ nhớ lưu trữ (server)
		PUT để cập nhật lại dữ liệu trong server
		GET để lấy dữ liệu từ server
		DELETE để xóa dữ liệu trong server

	Tuy nhiên, để có thể sử dụng HttpClient, ta nên gọi thông qua IHttpClientFactory.GetClient() bởi một số lí do
		Để gọi HttpClient, ta sẽ sử dụng từ khóa using để có thể tạo ra kết nối HTTP trong phương thức đó thôi. Khi chạy xong phương thức thì HttpClient sẽ bị loại bỏ. Tuy nhiên điều này có thể dẫn tới việc tạo ra quá nhiều HttpClient, giảm hiệu suất hệ thống

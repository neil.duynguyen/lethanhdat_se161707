Project này để học về lambda expression

__Lí_thuyết
    Ví dụ, khi truyền tham số là một phương thức vào phương thức khác,
        chúng ta sẽ cần định nghĩa trước phương thức đó
    VD:
        // Define method
        public void Add(int x, int y)
        {
            Console.WriteLine(@"The Sum of {0} and {1}, is {2} ", x, y, (x + y));
        }
        // Use delegate
        public delegate void AddDele(int a, int b);
	    // Assign method
        AddDele addDele = Add;

    Với annonymous function, chúng ta sẽ lược bớt phần tên phương thức đi
    VD:
         // Use delegate
        public delegate void AddDele(int a, int b);
	    // Define and assign method 
        AddDele addDele = delegate(int num1, int num2) 
            { Console.WriteLine(@"The Sum of {0} and {1}, is {2} ", x, y, (x + y));}

    Với lambda expression, chúng ta sẽ khai báo gọn hơn như sau
    VD:
        // Use delegate
        public delegate void AddDele(int a, int b);
	    // Define and assign method 
        AddDele addDele = (num1, num2) 
            => Console.WriteLine(@"The Sum of {0} and {1}, is {2} ", x, y, (x + y))

    Khi khai báo method và annonymous function, cần cú pháp đầy đủ
    Tuy nhiên khi khai báo lambda expression, có một số quy tắc giúp lambda expression gọn hơn

        //1. Có thể bỏ qua kiểu dữ liệu của parameter truyền vào
        (string input) => { //Code...}
        (input) => {//Code...}
 
        //2. Nếu không có parameter, bỏ dấu () trống
        () => {//Code...}
 
        //3. Nếu chỉ có 1 parameter, có thể bỏ luôn dấu ()
        (x) => {//Code...}
        x => {//Code...}
 
        //4. Nếu có nhiều parameter, ngăn cách bằng dấu phẩy
        (x, y) => {//Code...}
 
        //5. Nếu anonymous function chỉ có 1 câu lệnh, có thể bỏ dấu {}
        x => { //Code... }
        x => //Code...
 
        //6. Nếu chỉ return 1 giá trị, có thể bỏ chữ return.
        //4 lambda expression sau tương đương nhau
        (x) => { return x > 4; }
        x => { return x > 4; }
        x => return x > 4
        x => x > 4


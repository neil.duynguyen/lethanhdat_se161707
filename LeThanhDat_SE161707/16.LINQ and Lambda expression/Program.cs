﻿

public class Program
{
    delegate bool Condition(int element);
    static bool isCondition(int element)
    {
        return element > 10;
    }
    public static void Main()
    {

        var numberList = new List<int>()
            { 17, 2, 10, 3, 18, 4, 1, 9, 8, 19};
        
        // Passing delegate
        var resultList1 = GetListByCondition(numberList, isCondition);

        // Passing anonymous function
        var resultList2 = GetListByCondition(numberList, delegate(int number)
        {
            return number > 10;
        });
        // Passing lambda expression
        var resultList3 = GetListByCondition(numberList, number=>number>10);

        foreach (var result in resultList1) Console.WriteLine($"{result}, ");
        Console.WriteLine();
        foreach (var result in resultList2) Console.WriteLine($"{result}, ");
        Console.WriteLine();
        foreach (var result in resultList3) Console.WriteLine($"{result}, ");
        Console.WriteLine();

    }

    static List<int> GetListByCondition(List<int> list, Condition isCondition)
    {
        var resultList = new List<int>();
        foreach (int element in list)
        {
            if (isCondition(element)) resultList.Add(element);
        }

        return resultList;
    }
}
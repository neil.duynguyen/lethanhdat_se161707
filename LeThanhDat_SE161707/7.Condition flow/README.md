Project này để học về loops
__Định_nghĩa
	Loops (vòng lặp) giúp thực hiện lặp lại một đoạn chương trình nào đó đến khi thỏa điều kiện
	Vòng lặp giúp code ngắn gọn, tiết kiệm thời gian, giảm lỗi, giúp code dễ đọc
__Mục_tiêu

__Lí_thuyết
	Các loại vòng lặp
	while lặp lại đoạn code miễn còn thỏa điều kiện
	do while sẽ thực hiện đoạn code, rồi lặp lại miễn thỏa điều kiện
	
	Cấu trúc while (do while tương tự)
	while (điều kiện)
	{
		//code thực thi
	}


	vòng for sẽ lặp lại đoạn code với số lần hữu hạn
	Cấu trúc for
	for (giá trị ban đầu; điều kiện để vòng lặp chạy; code thực thi)
	{
		//code thực thi
	}

	Cụ thể, sau khi đặt giá trị ban đầu xong, nếu thỏa điều kiện ở 'điều kiện để vòng lặp chạy'
		vòng for sẽ tiếp tục

	Vòng lặp foreach sẽ thực hiện đoạn code liên tục ứng với số lượng phần tử trong array (mảng giá trị) 
	Cấu trúc foreach

	foreach (giá trị in mảng)
	{
		//code thực thi
	}
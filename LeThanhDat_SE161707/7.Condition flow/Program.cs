﻿
int age = 18;
//use if statemet
Console.WriteLine("using if-else");
if (age >= 18) Console.WriteLine("You are adult");
//use else statement right before if
else Console.WriteLine("You are children");

//use switch-case
Console.WriteLine("\nusing switch-case");
int month = 8;

switch (month)
{
    case 1:
        Console.WriteLine("Jan");
        break;
    case 2:
        Console.WriteLine("Feb");
        break;
    case 8:
        Console.WriteLine("Aug");
        break;
    default: Console.WriteLine("Not a month");
        break;
}




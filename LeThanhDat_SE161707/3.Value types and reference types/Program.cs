﻿//Declare a value type
int number = 5;

Console.WriteLine(number);
//Try to assign null to value type
//number = null;


//Declare a reference type
string name = "Dat UwU";

Console.WriteLine(name);

//Declare a nullable type
int? nullableNumber = 3;
Console.WriteLine(nullableNumber.Value);
//assign null value
name = null;
nullableNumber = null;

//use HasValue() to check if has value
if (!nullableNumber.HasValue) Console.Write("This number is null");
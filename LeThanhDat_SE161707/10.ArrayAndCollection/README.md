Project này để học về Collections
__Định_nghĩa
	Array là kiểu dữ liệu chứa các dữ liệu có cùng kiểu, theo thứ tự tuần tự
	Collection (tập hợp) là các lớp hỗ trợ lưu trữ, quản lí tập hợp các đối tượng
__Mục_tiêu

__Lí_thuyết
	Đặc điểm của array
		Chỉ lưu trữ dữ liệu cùng loại
		Các phần tử được truy cập bằng chỉ số (index). 
		Phần tử đầu tiên có chỉ số là 0 và phần tử cuối cùng có chỉ số là độ dài của mảng - 1
		Số lượng phần tử của mảng được khởi tạo ngay từ đầu và luôn cố định không đổi

	Đặc điểm của collections
		Có thể lưu trữ với kích thước động, so với array cần phải khai báo kích thước khi khởi tạo
		Có thể lưu trữ nhiều dữ liệu từ các kiểu dữ liệu khác nhau
		Có nhiều phương thức hỗ trợ làm việc với mảng
		Có thể truy cập bằng chỉ số thứ tự hoặc key

	Một số collections 
		List
			Là lớp mô tả một tập hợp gồm các đối tượng cố định không đổi, tương tự array nhưng kích thước linh động
			Có các phương thức để làm các công việc như thêm xóa sửa tìm kiếm
			Có thể truy cập vào từng phần tử bằng chỉ số
		SortedList
			Được tạo ra bằng các cặp từ khóa-giá trị và được sắp xếp theo từ khóa
			Khi ta thêm hoặc bớt một phần tử, sẽ sắp xếp lại danh sách này

			Thường dùng khi cần quản lí những danh sách nhỏ và cần được sắp xếp
			Với danh sách lớn, nên dùng dictionary, HashSet hoặc list
		Dictionary
			Được tạo ra bằng các cặp từ khóa-giá trị
			Các cặp khóa-giá trị không thể chứa khóa null hoặc khóa trùng lặp, 
				nhưng các giá trị trong danh sách vẫn được trùng lặp
		
		Stack
			Là kiểu dữ liệu thuộc Collections. Trong stack, phần tử nào mới được thêm vào danh sách sẽ được xóa đi đầu tiên
			Theo nguyên tắc LIFO (Last in first out)

		Queue
			Là kiểu dữ liệu thuộc Collections. Trong stack, phần tử nào được thêm vào danh sách đầu tiên sẽ được xóa đi đầu tiên
			Theo nguyên tắc FIFO (First in first out)
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10.ArrayAndCollection
{
    internal class Dictionary
    {
        public static void Run()
        {
            var MyDictionary = new Dictionary<string, Person>();

            //Add new element
            MyDictionary.Add(key: "P01", value: new Person
            {
                Name = "Dat UwU",
                Age = 18
            });
            MyDictionary.Add(key: "P02", value: new Person
            {
                Name = "Dat OwO",
                Age = 18
            });
            MyDictionary.Add(key: "P03", value: new Person
            {
                Name = "Admin OwO",
                Age = 18
            });

            //Retrieve Elements From a Dictionary
            foreach (KeyValuePair<string, Person> person in MyDictionary)
            {
                Console.WriteLine($"Key = {person.Key}, Value = {person.Value.Name}, {person.Value.Age}");
            }
            Console.WriteLine(MyDictionary.ElementAt(2));
            Console.WriteLine(MyDictionary.ElementAt(2).Key);
            Console.WriteLine(MyDictionary.GetValueOrDefault("P02"));

            //Update dictionary
            MyDictionary["P01"] = new Person
            {
                Name = "Le Thanh Dat OwO",
                Age = 18
            };
            Console.WriteLine(MyDictionary.GetValueOrDefault("P01"));

            //Delete from dictionary
            MyDictionary.Remove("P01");
            Console.WriteLine(MyDictionary.GetValueOrDefault("P01"));
        }
    }
}

﻿using System.Collections;

public class Generic
{
    public static void Learn()
    {
        // Method with generic
        void Swap<T>(T value1, T value2)
        {
            T temp = value1;
            value1 = value2;
            value2 = temp;
        }

        var list = new List<int>();

        list.Add(0);
        //list.Add("uwu"); //error

        Console.WriteLine(list[0]);

        var num1 = 1;
        var num2 = 3;
        Console.WriteLine($"Number before swap: {num1} {num2}");
        var float1 = 1.5f;
        var float2 = 9.5f;
        Console.WriteLine($"Float before swap: {float1} {float2}");
        //Using generic method
        Swap<int>(num1, num2);
        Swap<float>(float1, float2);
        Console.WriteLine($"Number after swap: {num1} {num2}");
        Console.WriteLine($"Float after swap: {float1} {float2}");


    }
}
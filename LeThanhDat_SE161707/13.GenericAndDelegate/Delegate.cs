﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class Delegate
{
    //Initialize delegate
    public delegate int NumberProcessDelegate(int num1, int num2);

    public static int addNum(int num1, int num2) => num1 + num2;
    public static void Learn()
    {
        NumberProcessDelegate addProcessor = new NumberProcessDelegate(addNum);
        NumberProcessDelegate addProcessor1 = addNum;
        NumberProcessDelegate multiplyProcessor = new NumberProcessDelegate((num1, num2) => num1 * num2);

        //Use delegate method
        Console.WriteLine($"Add delegate {addProcessor(5, 3)}");
        Console.WriteLine($"Add delegate1 {addProcessor1(5, 3)}");
        Console.WriteLine($"Multiply delegate {multiplyProcessor(6, 4)}");

        Console.WriteLine($"Pass add delegate to method {GetResult(5, 3, addProcessor)}");
        Console.WriteLine($"Pass multiply delegate to method {GetResult(5, 3, multiplyProcessor)}");
    }

    static int GetResult(int num1, int num2, NumberProcessDelegate numberProcessorDelegate)
    {
        return numberProcessorDelegate(num1, num2);
    }
}
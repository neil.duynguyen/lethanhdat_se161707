Project này để học về generic và delegate
__Định_nghĩa
	Generic trong C# cho phép bạn định nghĩa một hàm, một lớp mà không cần chỉ ra đối số kiểu dữ liệu là gì. 
		Tuỳ vào kiểu dữ liệu mà người dùng truyền vào thì nó sẽ hoạt động theo kiểu dữ liệu đó. 

	Delegate là những kiểu dữ liệu trong C# mà biến tạo ra từ nó chứa tham chiếu tới phương thức, 
		thay vì chứa giá trị hoặc chứa tham chiếu tới object của các class bình thường.

__Mục_tiêu

__Lí_thuyết
	Về generic
		Ví dụ bạn muốn hàm hoán đổi giá trị 2 số nguyên ta sẽ viết như sau
		public static void Swap(ref int a, ref int b)
		{
			int temp = a;
			a = b;
			b = temp;
		}

		Nếu bạn muốn đổi giá trị cho 2 số thực, 
			bạn sẽ phải viết lại giống vậy, nhưng khác kiểu đầu vào

		Khi đó bạn có thể viết phương thức dưới dạng generic như sau
		public static void Swap<T>(ref T a, ref T b)
		{
			T temp = a;
			a = b;
			b = temp;
		}

		Khi đó hàm Swap sẽ chạy và thay kí tự T thành kiểu dữ liệu int tương ứng
		VD: Swap<int>(ref a, ref b)

		Cũng tương tự, khi bạn muốn viết một class quản lí ds sinh viên và một cái qli ds giảng viên
			bạn có thể ghi
		class Person<T>
		{
			private T[] list;

			//Some management method
		}
		Khi đó lớp Person sẽ tùy vào kiểu dữ liệu T truyền vào sẽ chạy và 
		thay kí tự T thành kiểu dữ liệu int tương ứng

		Đặc điểm của Generic
			Giúp hạn chế viết code và tăng tính tái sử dụng
			Sau khi truyền kiểu dữ liệu vào generic, sẽ không thể gán giá trị khác kiểu dữ liệu đã khởi tạo
			VD
			var list = new List<int>();
			list.add("UwU"); //không chạy dc

	Về Delegate
		Delegate cho phép phương thức được truyền vào như một tham số (tương tự Javascript)
		Có thể dùng chồng các delegate với nhau
		Để khai báo Delegate, cần chỉ định kiểu dữ liệu trả về và các tham số truyền vào
		VD: 
		class Mammals {}  
		class Dogs : Mammals {}  //Dog kế thừa Mammal
  
		class Program  
		{  
			// Define the delegate.  
			public delegate Mammals HandlerMethod();  //Delegate này nhận hàm trả về Mammals
  
			public static Mammals MammalsHandler()  
			{  
				return null;  
			}  
  
			public static Dogs DogsHandler()  
			{  
				return null;  
			}  
  
			static void Test()  
			{  
				HandlerMethod handlerMammals = MammalsHandler;  
  
				// Covariance enables this assignment.  
				// HandlerMethod cần một hàm trả về Mammals, 
				// mà hàm DogsHandler trả về Dog, Dog cũng là Mammals nên hợp lệ
				HandlerMethod handlerDogs = DogsHandler;  
			}  
		}  
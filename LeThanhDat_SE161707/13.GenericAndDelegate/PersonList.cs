﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _13.GenericAndDelegate
{
    internal class PersonList<T> where T : class
    {
        //Tuỳ vào khai báo mà mảng này có thể chứa danh sách học sinh, hoặc danh sách nhân viên...
        private T[] items;

        public T[] Items
        {
            get { return items; }
        }

        public PersonList(int Size)
        {
            items = new T[Size];
        }

        public T GetByIndex(int Index)
        {
            // Nếu index vượt ra khỏi chỉ số phần tử của mảng thì ném ra ngoại lệ
            if (Index < 0 || Index >= items.Length)
            {
                throw new IndexOutOfRangeException();
            }
            else
            {
                return items[Index];
            }
        }

        public void SetItemValue(int Index, T Value)
        {
            if (Index < 0 || Index >= items.Length)
            {
                throw new IndexOutOfRangeException();
            }
            else
            {
                items[Index] = Value;
            }
        }
    }
}
﻿
// Create a Car class
class Car
{
    public string model;  // Create a field

    // Create a class constructor for the Car class
    public Car()
    {
        model = "Mustang"; // Set the initial value for model
    }

    //Constructor with parameter
    public Car(string modelName)
    {
        model = modelName; // Set the initial value for model
    }

    //Method in class
    public void Run()
    {
        Console.WriteLine("This car run");
    }

    static void Main(string[] args)
    {
        Car Ford = new Car();  // Create an object of the Car Class (this will call the constructor)
        Car Toyota = new Car("Sport");  
        Console.WriteLine(Ford.model);  // Print the value of model (Mustang)
        Toyota.Run(); //Call the object's method
    }
}

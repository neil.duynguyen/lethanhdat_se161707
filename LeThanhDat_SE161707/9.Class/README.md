Project này để học về Class
__Định_nghĩa
	Class (lớp) là bản thiết kế để có thể thiết kế và tạo ra đối tượng
	Để có thể thiết kế class, chúng ta cần đưa ra các thuộc tính (attribute/field) cần có trong một đối tượng,
		cũng như các hành vi (phương thức/method) của đối tượng đó
__Mục_tiêu

__Lí_thuyết
	Để tạo ra một class, ta sẽ dùng từ khóa class
	VD: Tạo ra lớp Car với biến color

	class Car
	{
		string color = "red";
	}

	Để tạo ra một object từ class, ta sẽ dùng từ khóa new

	VD: Tạo ra "một chiếc xe hơi" mới và lấy giá trị về màu sắc của chiếc xe này
	static void Main(string[] args)
	{
		Car myObj = new Car();
		Console.WriteLine(myObj.color);
	}

	Trong class, các field và các method được gọi là class member
		Trong đó field là biến được tạo ra trong class

		Để truy cập field, ta sẽ tạo ra đối tượng mới thuộc class này (tạo ra car mới)
			sau đó dùng cú pháp với dấu chấm (.)

		Vd:
		static void Main(string[] args)
		{
			Car myObj = new Car();
			Console.WriteLine(myObj.color);
		}

		Method 
		Trong class, method sẽ định nghĩa các hành vi của đối tượng có thể làm
			(VD: Đối tượng oto có thể chạy hoặc dừng)

			Method có thể static để có thể truy cập mà không cần tạo class mới
			Nếu method không có từ khóa public, thì chỉ có thể truy cập method trong chính class đó thôi

			Có một method đặc biệt của class là Constructor
				Method này dùng để khởi tạo các giá trị ban đầu cho đối tượng mới

			VD:
			class Car
			{
			  public string model;  // Create a field

			  // Create a class constructor for the Car class
			  public Car()
			  {
				model = "Mustang"; // Set the initial value for model
			  }

			  public Car(string modelName)
			  {
				model = modelName; // Set the initial value for model
			  }

			  static void Main(string[] args)
			  {
				Car Ford = new Car();  // Create an object of the Car Class (this will call the constructor)
				Car Toyota = new Car("Sport")
				Console.WriteLine(Ford.model);  // Print the value of model (Mustang)
				Console.WriteLine(Toyota.model);  // Print the value of model (Sport)
			  }
			}
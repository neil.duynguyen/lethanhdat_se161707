﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _14.SortAlgorithms
{
    internal class RadixSorter
    {
        public static List<int> Sort(List<int> list)
        {
            var listSize = list.Count;
            var maxVal = GetMaxVal(list, listSize);
            for (int exponent = 1; maxVal / exponent > 0; exponent *= 10)
                CountingSort(list, listSize, exponent);
            return list;
        }

        public static int GetMaxVal(List<int> list, int size)
        {
            var maxVal = list[0];
            for (int i = 1; i < size; i++)
                if (list[i] > (maxVal))
                    maxVal = list[i];
            return maxVal;
        }

        public static void CountingSort(List<int> list, int size, int exponent)
        {
            var outputArr = new List<int>(size);
            var digitOccurences = new List<int>(10);

            
            for (int i = 0; i < 10; i++)
            {
                digitOccurences.Add(0);
                outputArr.Add(0);
            }
                

            for (int i = 0; i < size; i++)
                digitOccurences[(list[i] / exponent) % 10]++;

            // Decide where to place digit in the list
            for (int i = 1; i < 10; i++)
                digitOccurences[i] += digitOccurences[i - 1];

            // Scan the array, get the value digit base on exponent
            // Base on occurence array, with index equal digit, we get a value from occurence array
            // which is position for next value
            for (int i = size - 1; i >= 0; i--)
            {
                var index = digitOccurences[(list[i] / exponent) % 10] - 1;
                var value = list[i];
                digitOccurences[(list[i] / exponent) % 10]--; 
                
                // Next time, if value with that digit found again
                // Place it on the left of value with that same digit
                outputArr[index] = value;
            }

            for (int i = 0; i < size; i++)
                list[i] = outputArr[i];
        }
    }
}

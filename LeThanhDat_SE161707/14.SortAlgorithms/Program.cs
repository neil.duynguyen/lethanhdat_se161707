﻿// See https://aka.ms/new-console-template for more information
using _14.SortAlgorithms;

var numberList1 = new List<int>()
{
    10, 86, 22, 96, 23, 72, 52, 33, 98, 42
} ;
var numberList2 = new List<int>()
{
    10, 86, 22, 96, 23, 72, 52, 33, 98, 42
};
foreach (int number in numberList1)
{
    Console.Write(number.ToString() + ", ");
}

Console.WriteLine();
Console.WriteLine("Shell sort:");

foreach (int number in ShellSorter.Sort(numberList1))
{
    Console.Write(number.ToString() + ", ");
}

Console.WriteLine();
Console.WriteLine("Radix sort:");
foreach (int number in RadixSorter.Sort(numberList2))
{
    Console.Write(number.ToString() + ", ");
}




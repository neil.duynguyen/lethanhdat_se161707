﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _14.SortAlgorithms
{
    internal class ShellSorter 
    {
        public static List<T> Sort<T>(List<T> list) where T : IComparable<T>
        {
            var listLength = list.Count;
            for (var interval = listLength / 2; interval > 0; interval /= 2)
            {                
                for (var currentLocation = interval; currentLocation < listLength; currentLocation++)
                {
                    var currentValue = list[currentLocation];
                    var currentIndex = currentLocation;


                    //While current value greater than prev value and not reach the start of array
                    while (currentIndex >= interval && list[currentIndex - interval].CompareTo(currentValue) > 0)
                    {
                        //Push previous value to current value
                        list[currentIndex] = list[currentIndex - interval];
                        //Go to previous index to push value
                        currentIndex -= interval;

                        
                    }

                    list[currentIndex] = currentValue;
                }
            }
            return list;
        }

    }
}

Project này để học về concept cơ bản
__Định_nghĩa
	CLI là giao diện sử dụng dòng lệnh để tương tác với máy tính, quản lí file
	Compiler là biên dịch, tức sẽ dịch hết dự án rồi chạy, khi đó nếu có lỗi cú pháp, toàn bộ chương trình không chạy dc. 
	Interpreter là phiên dịch, tức sẽ chạy dòng lệnh sau khi dòng đó được dịch, nếu chương trình có lỗi, thì các dòng trước đó vẫn chạy bình thường

__Mục_tiêu

__Lí_thuyết
	Một project C# gồm nhiều file, mỗi file có thể chứa các namespace
	Namespace được thiết kế để tổ chức và phân nhóm toàn bộ các kiểu dữ liệu theo cấu trúc phân cấp, giúp tránh xung đột tên
	C# là ngôn ngữ vừa biên dịch (compiler) và vừa phiên dịch (interpreter)


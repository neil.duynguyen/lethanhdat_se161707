Project này để học về concept cơ bản
__Định_nghĩa
	String là kiểu dữ liệu dùng để lưu trữ văn bản
	String là tập hợp nhiều kí tự, nằm trong dấu ngoặc kép


__Lí_thuyết
	Một số phương thức để làm việc với string
	ToLower()
	ToUpper() để in hoa chuỗi
	Concat() để nối 2 chuỗi với nhau
	Length để lấy độ dài của chuỗi



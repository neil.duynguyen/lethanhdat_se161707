﻿// Create a variable of type string and assign it a value
string text = "Hello, World!";

Console.Write("Text: "); Console.WriteLine(text);
// Some method to work with string
Console.Write("Text length: "); Console.WriteLine(text.Length);
Console.Write("Text in upper case: "); Console.WriteLine(text.ToUpper());
Console.Write("Text in lower case: "); Console.WriteLine(text.ToLower());
Console.Write("Text contains \"World\"? "); Console.WriteLine(text.Contains("World"));

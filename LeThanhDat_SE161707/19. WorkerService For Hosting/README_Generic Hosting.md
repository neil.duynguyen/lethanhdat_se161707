Project này để học về generic host
__Lí_thuyết
	Host là một đối tượng, sẽ đóng gói các tài nguyên của app, bao gồm
		Dependency Injection,
		Logging,
		Configuration,
		Lớp được triển khai từ IHostedService

	Khi khởi động host lên, thì host sẽ chạy phương thức IHostedService.StartService() từ các lớp được triển khai từ IHostedService
	
	Để cài đặt một host, chúng ta sẽ chuẩn bị code ở Program.cs như sau

		await Host.CreateDefaultBuilder(args)
		.ConfigureServices(services =>
		{
			// Đưa các host service cần triển khai vào đây
			services.AddHostedService<PrintTextToConsoleService>();
			// services.AddHostedService<SampleHostedService2>();
		})
		.Build()
		.RunAsync();

	Ngoài ra, các hosted service sẽ được triển khai như sau
	VD
		public class PrintTextToConsoleService : IHostedService // Implement IHostedService
		{
		  private readonly ILogger _logger;
		  private readonly IOptions<AppConfig> _appConfig;
		  private Timer _timer;

		  public PrintTextToConsoleService(ILogger<PrintTextToConsoleService> logger, IOptions<AppConfig> appConfig)
		  {
			_logger = logger;
			_appConfig = appConfig;
		  }

		  // ======Need a StartAsync to run host======
		  public Task StartAsync(CancellationToken cancellationToken)
		  {
			_logger.LogInformation("Starting");

			_timer = new Timer(DoWork, null, TimeSpan.Zero,
			  TimeSpan.FromSeconds(5));

			return Task.CompletedTask;
		  }

		  private void DoWork(object state)
		  {
			_logger.LogInformation($"Background work with text: {_appConfig.Value.TextToPrint}");
		  }

		  public Task StopAsync(CancellationToken cancellationToken)
		  {
			_logger.LogInformation("Stopping.");

			_timer?.Change(Timeout.Infinite, 0);

			return Task.CompletedTask;
		  }

		  public void Dispose()
		  {
			_timer?.Dispose();
		  }
		}

	Ngoài ra, trước khi khởi chạy host, chúng ta có thể khởi tạo thông số cho Host từ JSON, từ biến môi trường, từ command line...
	VD:
		Host.CreateDefaultBuilder(args)
		.ConfigureHostConfiguration(hostConfig =>
		{
			hostConfig.SetBasePath(Directory.GetCurrentDirectory());
			hostConfig.AddJsonFile("hostsettings.json", optional: true);
			hostConfig.AddEnvironmentVariables(prefix: "PREFIX_");
			hostConfig.AddCommandLine(args);
		});

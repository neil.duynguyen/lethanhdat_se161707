
using Microsoft.AspNetCore.Hosting;
using System.Reflection;
using System.Reflection.PortableExecutable;
using Microsoft.AspNetCore.Hosting;

IHost host = Host.CreateDefaultBuilder(args)
    //.UseEnvironment("Development")
    .ConfigureServices((_, services) =>
    {
        // Use this for dependency injection
        services.AddHostedService<HostApplicationLifetimeEventsHostedService>();
        services.AddRazorPages();
    })
    .ConfigureWebHostDefaults(webBuilder =>
    {
        // Configure for web host here
        webBuilder.Configure((ctx, app) =>
        {
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", () => "Hello World!");
                endpoints.MapRazorPages();
            });
        });
    })
    .ConfigureHostConfiguration((hostConfig) =>
    {
        // Configure for host here
        hostConfig.SetBasePath(Directory.GetCurrentDirectory());
        hostConfig.AddJsonFile(path: "hostsettings.json", optional: true, reloadOnChange: true); // Add config from JSON file
        hostConfig.AddEnvironmentVariables(prefix: "ASPNETCORE_"); // Add config from environment variable with given prefix
        hostConfig.AddCommandLine(args); // Allow add config when running with command line
    })
    .ConfigureAppConfiguration((hostingContext, config) =>
    {
        // App config go here

    })
    .Build();

host.Run();
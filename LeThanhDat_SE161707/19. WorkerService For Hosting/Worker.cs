using Microsoft.Extensions.Configuration;

public class HostApplicationLifetimeEventsHostedService : IHostedService
{
    private readonly IHostApplicationLifetime _hostApplicationLifetime;
    private readonly ILogger _logger;
    private readonly IConfiguration _configuration;

    public HostApplicationLifetimeEventsHostedService(
        IHostApplicationLifetime hostApplicationLifetime, 
        ILogger<HostApplicationLifetimeEventsHostedService> logger,
        IConfiguration configuration)
    {
        _hostApplicationLifetime = hostApplicationLifetime;
        _logger = logger;

        //Inject IConfiguration here to use config
        _configuration = configuration;
    }

    public Task StartAsync(CancellationToken cancellationToken)
    {
        _hostApplicationLifetime.ApplicationStarted.Register(OnStarted);

        _hostApplicationLifetime.ApplicationStopping.Register(KhiBatDauDungLai);
        _hostApplicationLifetime.ApplicationStopped.Register(OnStopped);

        _logger.LogInformation("1. StartAsync has been called.");

        return Task.CompletedTask;
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("4. StopAsync has been called.");

        return Task.CompletedTask;

    }

    private void OnStarted()
    {
        // ...
        _logger.LogInformation("2. OnStarted has been called.");

        // Get config by key
        _logger.LogCritical("ASPNETCORE_ENVIRONMENT: " + _configuration["environmentVariables:ASPNETCORE_ENVIRONMENT"]);
        _logger.LogCritical("ASPNETCORE_APPLICATIONNAME: " + _configuration["environmentVariables:ASPNETCORE_APPLICATIONNAME"]);
        _logger.LogCritical("ASPNETCORE_SHUTDOWNTIMEOUTSECONDS: " + _configuration["environmentVariables:ASPNETCORE_SHUTDOWNTIMEOUTSECONDS"]);
    }

    private void KhiBatDauDungLai()
    {
        // ...
        _logger.LogInformation("3. OnStopping has been called.");
    }

    private void OnStopped()
    {
        // ...
        _logger.LogInformation("5. OnStopped has been called.");
    }
}
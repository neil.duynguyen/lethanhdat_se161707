﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

public class Index2Model : PageModel
{
    private IConfigurationRoot ConfigRoot;
    private readonly ILogger<Index2Model> _logger;

    public Index2Model(
        IConfiguration configRoot,
        ILogger<Index2Model> logger)
    {
        ConfigRoot = (IConfigurationRoot)configRoot;
        _logger = logger;
    }

    public ContentResult OnGet()
    {
        string str = "";
        foreach (var provider in ConfigRoot.Providers.ToList())
        {
            str += provider.ToString() + "\n";
        }

        _logger.LogInformation(str);
        return Content(str);
    }
}
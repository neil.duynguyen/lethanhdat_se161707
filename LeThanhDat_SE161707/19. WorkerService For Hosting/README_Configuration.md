Project này để học về Configuration
__Lí_thuyết
	Để cấu hình cho ứng dụng ASP.NET Core, chúng ta sẽ cần dùng một hoặc nhiều các phương pháp cấu hình:
		Settings files, such as appsettings.json (Chủ yếu đề cập tới cái này)
		Environment variables (Chủ yếu đề cập tới cái này)
		Azure Key Vault
		Azure App Configuration
		Command-line arguments
		Custom providers, installed or created
		Directory files
		In-memory .NET objects

	Ví dụ file hostsettings.json có cấu hình như sau
	hostsettings.json
		{
		  "environmentVariables": {
			"ASPNETCORE_ENVIRONMENT": "Development",
			"ASPNETCORE_SHUTDOWNTIMEOUTSECONDS": "102",
			"ASPNETCORE_APPLICATIONNAME": "19. WorkerService For Hosting"
		  }
		}

	Khi đó có thể đọc dữ liệu từ file json đó như sau (Cú pháp có thể khác tùy vào dự án)
	VD
		using Microsoft.Extensions.DependencyInjection.ConfigSample.Options;

		var builder = WebApplication.CreateBuilder(args);

		builder.Configuration.AddJsonFile("MyConfig.json",
				optional: true,
				reloadOnChange: true);

		builder.Services.AddRazorPages();

		var app = builder.Build();

	Đối với biến môi trường, có thể cấu hình trước ở trong dự án tại Properties/launchSetting.json
	VD
		{
		  "profiles": {
			"PROJECT_NAME": {
			  "commandName": "Project",
			  "environmentVariables": {
				// define environment variables here
			  },
			  "dotnetRunMessages": true
			}
		  }
		}

	Để sử dụng biến môi trường, ta sẽ gọi phương thức sau
		 AddEnvironmentVariables(prefix: "SOMEPREFIX_");

	Để lấy giá trị từ file json và sử dụng, ta sẽ inject IConfiguration vào để có thể sử dụng
	VD: 
		public class TestModel : PageModel
		{
			// requires using Microsoft.Extensions.Configuration;
			private readonly IConfiguration Configuration;

			public TestModel(IConfiguration configuration)
			{
				Configuration = configuration;
			}

			public ContentResult OnGet()
			{
				// Get value from json here
				var myKeyValue = Configuration["MyKey"];
				var title = Configuration["Position:Title"];
				var name = Configuration["Position:Name"];
				var defaultLogLevel = Configuration["Logging:LogLevel:Default"];


				return Content($"MyKey value: {myKeyValue} \n" +
							   $"Title: {title} \n" +
							   $"Name: {name} \n" +
							   $"Default Log Level: {defaultLogLevel}");
			}
		}
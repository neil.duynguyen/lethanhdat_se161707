Project này để học về 4 tinhs chat cua OOP
__Định_nghĩa
	
__Mục_tiêu

__Lí_thuyết
	"Abstraction là việc ẩn đi các chi tiết triển khai bên trong, chỉ hiển thị những gì cần thiết ra bên ngoài.

	Nói cách khác, người dùng chỉ cần biết có thể thực hiện những gì (what), còn việc thực hiện ra sao thì không cần quan tâm (how).
	Tính trừu tượng giúp bạn tập trung vào những cốt lõi cần thiết của đối tượng thay vì quan tâm đến cách nó thực hiện

	Cách để đạt được tính trừu tượng chính là thông qua interface và abstract class"

	"Encapsulation
	Các dữ liệu và phương thức có liên quan với nhau được đóng gói thành các lớp (class) để tiện cho việc quản lý và sử dụng.
	Ví dụ: cân nặng, chiều cao,... và hành động nhìn, sủa, vẫy đuôi của chó sẽ được đóng gói lại thành một lớp tên là Chó.
	 Ngoài ra, đóng gói còn để che giấu một số thông tin và chi tiết cài đặt nội bộ để bên ngoài không thể nhìn thấy. (Thông qua các phạm vi truy cập - access modifier (public, private, protected, internal, protected internal))"

	"Inheritance
	Tính kế thừa là khả năng cho phép ta xây dựng một lớp mới dựa trên các định nghĩa của một lớp đã có. 
	Lớp đã có gọi là lớp Cha, lớp mới phát sinh gọi là lớp Con và đương nhiên kế thừa tất cả các thành phần của lớp Cha, có thể chia sẻ hay mở rộng các đặc tính sẵn có mà không phải tiến hành định nghĩa lại."

	"Polymorphism
	Khi một tác vụ được thực hiện theo nhiều cách khác nhau được gọi là tính đa hình.
	Trong Java và C#, chúng ta dùng overiding và overloading để có tính đa hình
	Ghi đè (Overriding): là hai phương thức cùng tên, cùng tham số, cùng kiểu trả về nhưng thằng con viết lại và dùng theo cách của nó, và xuất hiện ở lớp cha và tiếp tục xuất hiện ở lớp con. Khi dùng override, lúc thực thi, nếu lớp Con không có phương thức riêng, phương thức của lớp Cha sẽ được gọi, ngược lại nếu có, phương thức của lớp Con được gọi.
	Nạp chồng (Overloading): Đây là khả năng cho phép một lớp có nhiều thuộc tính, phương thức cùng tên nhưng với các tham số khác nhau về loại cũng như về số lượng. Khi được gọi, dựa vào tham số truyền vào, phương thức tương ứng sẽ được thực hiện."
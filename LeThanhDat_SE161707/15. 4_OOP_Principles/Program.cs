﻿
using _15._4_OOP_Principles.Student;

// Create 2 student objects
Student abStudent = new NaturalScienceStudent();
Student adStudent = new SocialScienceStudent();

// Call same method but different class
Console.WriteLine("Student class ab: ");
abStudent.LearnSpecialization();
Console.WriteLine("Student class ad: ");
adStudent.LearnSpecialization();


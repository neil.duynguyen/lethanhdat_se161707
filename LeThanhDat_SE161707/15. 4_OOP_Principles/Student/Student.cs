﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _15._4_OOP_Principles.Student
{
    public abstract class Student
    {

        // Encapsulation
        // Set name, age in private, so these data cannot edit directly
        private string name;
        private int yearOfBirth;
        private int currentYear = 2023;

        // Use getter, setter to access and modify these data
        public string getName() => name;
        // This getter process data before public out
        public int getAge() => currentYear - yearOfBirth;
        public void setName(string name) { this.name = name; }
        public void setYearOfBirth(int yearOfBirth) { this.yearOfBirth = yearOfBirth; }


        // Method use in common
        public void LearnGeneral()
        {
            Console.WriteLine("Learn Physical Education");
            Console.WriteLine("Learn Music");
        }


        // Abstaction
        // We know that student learn at school, but we don't know how each type of student learn actually
        public abstract void LearnSpecialization();
    }
}

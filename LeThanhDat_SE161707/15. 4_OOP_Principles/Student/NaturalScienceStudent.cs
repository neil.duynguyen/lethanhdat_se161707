﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _15._4_OOP_Principles.Student
{
    // Inheritance
    // We can create a new type of student, use base info from Student class, and add extra behavior and attribute
    internal class NaturalScienceStudent : Student
    {
        // Polymorphism
        // By overload method, a method will behave differently base on object's class
        public override void LearnSpecialization()
        {
            Console.WriteLine("Learn Math");
            Console.WriteLine("Learn Chemistry");
            Console.WriteLine("Learn Physic");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _15._4_OOP_Principles.Student
{
    internal class SocialScienceStudent : Student
    {
        // Polymorphism
        // By overload method, a method will behave differently base on object's class
        public override void LearnSpecialization()
        {
            Console.WriteLine("Learn Literature");
            Console.WriteLine("Learn History");
            Console.WriteLine("Learn English");
        }
    }
}

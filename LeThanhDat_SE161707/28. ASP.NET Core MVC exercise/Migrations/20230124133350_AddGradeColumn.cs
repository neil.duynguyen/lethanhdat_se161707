﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace _28._ASP.NET_Core_MVC_exercise.Migrations
{
    public partial class AddGradeColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Grade",
                table: "Student",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Grade",
                table: "Student");
        }
    }
}

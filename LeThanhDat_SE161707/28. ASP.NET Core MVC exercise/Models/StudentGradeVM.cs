﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace _28._ASP.NET_Core_MVC_exercise.Models
{
    public class StudentGradeVM
    {
        public List<Student>? Students { get; set; }
        public SelectList? Grades { get; set; }
        public string? StudentGrade { get; set; }
        public string? SearchString { get; set; }
    }
}

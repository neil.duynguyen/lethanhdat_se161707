﻿using System.ComponentModel.DataAnnotations;

namespace _28._ASP.NET_Core_MVC_exercise.Models
{
    public class Student
    {
        public int Id { get; set; }
        [Display(Name = "First Name")]
        [StringLength(20, MinimumLength = 3)]
        [Required]
        public string FirstName { get; set; }
        [StringLength(30, MinimumLength = 3)]
        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Range(10,12)]
        [Required]
        public string Grade { get; set; }
        [Display(Name = "Enroll Date")]
        [DataType(DataType.Date)]
        public DateTime EnrollDate { get; set; }
    }
}

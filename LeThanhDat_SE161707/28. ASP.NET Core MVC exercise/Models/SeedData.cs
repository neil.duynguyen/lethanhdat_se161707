﻿using _28._ASP.NET_Core_MVC_exercise.Data;
using Microsoft.EntityFrameworkCore;

namespace _28._ASP.NET_Core_MVC_exercise.Models
{
    public class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new MvcSchoolContext(
    serviceProvider.GetRequiredService<DbContextOptions<MvcSchoolContext>>()))
            {
                if (context.Student.Any())
                {
                    return;   // DB has been seeded
                }

                context.Student.AddRange(
                    new Student
                    {
                        FirstName = "Dat",
                        LastName = "Le Thanh",
                        EnrollDate = DateTime.Parse("1989-2-12"),
                    },

                    new Student
                    {
                        FirstName = "Hieu",
                        LastName = "Tran Le Cong",
                        EnrollDate = DateTime.Parse("1989-2-13"),
                    },

                    new Student
                    {
                        FirstName = "Huyen",
                        LastName = "Doan Thi Thanh",
                        EnrollDate = DateTime.Parse("1989-10-23"),
                    },

                    new Student
                    {
                        FirstName = "OwO",
                        LastName = "Doan Xem",
                        EnrollDate = DateTime.Parse("1989-10-23"),
                    }
                );
                context.SaveChanges();
            }
        }
    }
}

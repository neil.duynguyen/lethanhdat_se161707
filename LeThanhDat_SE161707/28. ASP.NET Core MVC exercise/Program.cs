﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using _28._ASP.NET_Core_MVC_exercise.Data;
using _28._ASP.NET_Core_MVC_exercise.Models;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddDbContext<MvcSchoolContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("MvcSchoolContext") ?? throw new InvalidOperationException("Connection string 'SchoolContext' not found.")));

// Add services to the container.
builder.Services.AddControllersWithViews();

var app = builder.Build();

using (var scope = app.Services.CreateScope())
{
    var services = scope.ServiceProvider;

    SeedData.Initialize(services);
}
// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Students}/{action=Index}/{id?}");

app.Run();

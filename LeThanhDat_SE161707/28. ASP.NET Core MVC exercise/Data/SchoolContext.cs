﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using _28._ASP.NET_Core_MVC_exercise.Models;

namespace _28._ASP.NET_Core_MVC_exercise.Data
{
    public class MvcSchoolContext : DbContext
    {
        public MvcSchoolContext (DbContextOptions<MvcSchoolContext> options)
            : base(options)
        {
        }

        public DbSet<_28._ASP.NET_Core_MVC_exercise.Models.Student> Student { get; set; } = default!;
    }
}
